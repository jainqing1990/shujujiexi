package com.example.shujujiexi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShujujiexiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShujujiexiApplication.class, args);
    }

}
