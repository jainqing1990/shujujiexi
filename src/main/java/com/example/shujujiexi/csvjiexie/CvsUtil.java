package com.example.shujujiexi.csvjiexie;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CvsUtil {


        /**
         * 导入
         *
         * @param file csv文件(路径+文件)
         * @return
         */
        public static Map<String,List<String>> importCsv(DataInputStream  file){
            Map<String,List<String>> maplist=new HashMap<>();
            BufferedReader br=null;
            try {
                br = new BufferedReader(new InputStreamReader(file,"UTF-8"));
                String line = "";
                while ((line = br.readLine()) != null) {
                    String[] butstr=line.split("\\,");
                    String mapkey=butstr[1]+"|"+butstr[2];
                    List<String> list=maplist.get(mapkey);
                    if(list==null||"".equals(list)){
                        list=new ArrayList<>();
                    }
                    list.add(line);
                    maplist.put(mapkey,list);
                    System.out.println(line);
                }
            }catch (Exception e) {
                e.printStackTrace();
            }finally{
                if(br!=null){
                    try {
                        br.close();
                        br=null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return maplist;
        }


    /**
     * 功能说明：获取UTF-8编码文本文件开头的BOM签名。
     * BOM(Byte Order Mark)，是UTF编码方案里用于标识编码的标准标记。例：接收者收到以EF BB BF开头的字节流，就知道是UTF-8编码。
     * @return UTF-8编码文本文件开头的BOM签名
     */
    public static String getBOM() {

        byte b[] = {(byte)0xEF, (byte)0xBB, (byte)0xBF};
        return new String(b);
    }

    /**
     * 生成CVS文件
     * @param exportData
     *       源数据List
     *       csv文件的列表头map
     *       文件路径
     *       文件名称
     * @return
     */
    public static boolean createCSVFile(Map<String,List<String>> exportData,String outpath) {
        boolean ifshow=false;
        FileWriter  csvFileOutputStream = null;
        for(String list:exportData.keySet()) {
            try {
                String[] keystr=list.replaceAll("\"","").split("\\|");
                List<String> liststr=exportData.get(list);
            File file = new File(outpath+keystr[0]);
            if (!file.exists()) {
                file.mkdirs();
            }
                file =new File(outpath+keystr[0]+"/"+keystr[1]+".csv");
//                if(!file.exists()){
//                    file.createNewFile();
//                }
                // UTF-8使正确读取分隔符","
                //如果生产文件乱码，windows下用gbk，linux用UTF-8
                csvFileOutputStream = new FileWriter(file, true);

                //写入前段字节流，防止乱码
                csvFileOutputStream.append(getBOM());
                for (String buffer : liststr) {
                    csvFileOutputStream.append(buffer + "\n");
                }
                ifshow = true;
            } catch (Exception e) {
                e.printStackTrace();
                ifshow = false;
            } finally {
                try {
                    csvFileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ifshow;
    }

    public static boolean createCSVFileDddd(Map<String,List<String>> exportData,String outpath) {
        boolean ifshow=false;
        FileWriter  csvFileOutputStream = null;
        for(String list:exportData.keySet()) {
            try {
                String[] keystr=list.replaceAll("\"","").split("\\|");
                List<String> liststr=exportData.get(list);
                File file = new File(outpath+keystr[0]);
                if (!file.exists()) {
                    file.mkdirs();
                }
                file =new File(outpath+keystr[0]+"/"+keystr[1]+".csv");
//                if(!file.exists()){
//                    file.createNewFile();
//                }
                // UTF-8使正确读取分隔符","
                //如果生产文件乱码，windows下用gbk，linux用UTF-8
                csvFileOutputStream = new FileWriter(file, true);

                //写入前段字节流，防止乱码
                csvFileOutputStream.append(getBOM());
                Integer isr=0,pages=0;
                for (String buffer : liststr) {
                    isr++;
                    if(isr>=500000){
                        pages++;
                        file =new File(outpath+keystr[0]+"/"+keystr[1]+pages+".csv");
                        csvFileOutputStream = new FileWriter(file, true);
                        csvFileOutputStream.append(getBOM());
                        isr=0;
                    }
                    csvFileOutputStream.append(buffer + "\n");
                }
                ifshow = true;
            } catch (Exception e) {
                e.printStackTrace();
                ifshow = false;
            } finally {
                try {
                    csvFileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ifshow;
    }

}
